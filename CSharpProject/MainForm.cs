using CSharpProject.Entities;
using CSharpProject.Helpers;
using CSharpProject.Persistence;
using CSharpProject.Persistence.Abstraction;

namespace CSharpProject
{
    public partial class MainForm : Form
    {
        private StudentPersistor _studentPersistor = new StudentPersistor();
        private string _selectedStudentId = null;

        private LecturerPersistor _lecturerPersistor = new LecturerPersistor();
        private string _selectedLecturerId = null;

        private CoursePersistor _coursePersistor = new CoursePersistor();
        private string _selectedCourseId = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void createStudentButton_Click(object sender, EventArgs e)
        {
            var student = new Student()
            {
                FirstName = studentFirstNameTextBox.Text,
                LastName = studentLastNameTextBox.Text,
                DateOfBirth = DateTime.Parse(studentDateOfBirthTextBox.Text),
                Address = studentAddressTextBox.Text
            };

            _studentPersistor.Create(student);
            ReloadStudentsView();
        }
        private void updateStudentButton_Click(object sender, EventArgs e)
        {
            var student = new Student()
            {
                FirstName = studentFirstNameTextBox.Text,
                LastName = studentLastNameTextBox.Text,
                DateOfBirth = DateTime.Parse(studentDateOfBirthTextBox.Text),
                Address = studentAddressTextBox.Text
            };
            _studentPersistor.Update(_selectedStudentId, student);
            ReloadStudentsView();
        }

        private void deleteStudentButton_Click(object sender, EventArgs e)
        {
            _studentPersistor.Delete(_selectedStudentId);
            ReloadStudentsView();
        }

        private void ReloadStudentsView()
        {
            studentDbList.Items.Clear();
            foreach (var item in _studentPersistor.GetAll())
            {
                studentDbList.Items.Add(item);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ReloadStudentsView();
        }

        private void studentDbList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = (Student)studentDbList.Items[studentDbList.SelectedIndex];
            _selectedStudentId = selectedItem.Id;
            studentFirstNameTextBox.Text = selectedItem.FirstName;
            studentLastNameTextBox.Text = selectedItem.LastName;
            studentDateOfBirthTextBox.Text = selectedItem.DateOfBirth.ToFormattedString();
            studentAddressTextBox.Text = selectedItem.Address;
        }
    }
}