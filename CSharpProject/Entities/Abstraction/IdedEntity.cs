﻿namespace CSharpProject.Entities.Abstraction
{
    public abstract class IdedEntity
    {
        public string Id { get; set; }
    }
}
