﻿using CSharpProject.Entities.Abstraction;

namespace CSharpProject.Entities
{
    public class Course : IdedEntity // Kurs
    {
        public List<string> StudentIds { get; set; } = new List<string>();
    }
}
