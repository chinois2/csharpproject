﻿using CSharpProject.Entities.Abstraction;

namespace CSharpProject.Entities
{
    public class Student : Person // Student
    {
        public override string ToString()
        {
            return $"Name: {this.FirstName} {this.LastName}, Date of birth: {this.DateOfBirth.ToString("dd.MM.yyyy")}, Address: {this.Address}";
        }
    }
}
