﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpProject.Helpers
{
    public static class DateFormatter
    {
        public static string ToFormattedString(this DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }
    }
}
