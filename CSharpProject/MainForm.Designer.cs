﻿namespace CSharpProject
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createStudentButton = new System.Windows.Forms.Button();
            this.studentDbList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.studentFirstNameTextBox = new System.Windows.Forms.TextBox();
            this.studentLastNameTextBox = new System.Windows.Forms.TextBox();
            this.studentDateOfBirthTextBox = new System.Windows.Forms.TextBox();
            this.studentAddressTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.updateStudentButton = new System.Windows.Forms.Button();
            this.deleteStudentButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // createStudentButton
            // 
            this.createStudentButton.Location = new System.Drawing.Point(490, 141);
            this.createStudentButton.Name = "createStudentButton";
            this.createStudentButton.Size = new System.Drawing.Size(138, 23);
            this.createStudentButton.TabIndex = 0;
            this.createStudentButton.Text = "Create Student";
            this.createStudentButton.UseVisualStyleBackColor = true;
            this.createStudentButton.Click += new System.EventHandler(this.createStudentButton_Click);
            // 
            // studentDbList
            // 
            this.studentDbList.FormattingEnabled = true;
            this.studentDbList.ItemHeight = 15;
            this.studentDbList.Location = new System.Drawing.Point(12, 27);
            this.studentDbList.Name = "studentDbList";
            this.studentDbList.Size = new System.Drawing.Size(429, 184);
            this.studentDbList.TabIndex = 1;
            this.studentDbList.SelectedIndexChanged += new System.EventHandler(this.studentDbList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Student Db Content";
            // 
            // studentFirstNameTextBox
            // 
            this.studentFirstNameTextBox.Location = new System.Drawing.Point(526, 25);
            this.studentFirstNameTextBox.Name = "studentFirstNameTextBox";
            this.studentFirstNameTextBox.Size = new System.Drawing.Size(100, 23);
            this.studentFirstNameTextBox.TabIndex = 3;
            // 
            // studentLastNameTextBox
            // 
            this.studentLastNameTextBox.Location = new System.Drawing.Point(526, 54);
            this.studentLastNameTextBox.Name = "studentLastNameTextBox";
            this.studentLastNameTextBox.Size = new System.Drawing.Size(100, 23);
            this.studentLastNameTextBox.TabIndex = 4;
            // 
            // studentDateOfBirthTextBox
            // 
            this.studentDateOfBirthTextBox.Location = new System.Drawing.Point(526, 83);
            this.studentDateOfBirthTextBox.Name = "studentDateOfBirthTextBox";
            this.studentDateOfBirthTextBox.Size = new System.Drawing.Size(100, 23);
            this.studentDateOfBirthTextBox.TabIndex = 5;
            // 
            // studentAddressTextBox
            // 
            this.studentAddressTextBox.Location = new System.Drawing.Point(526, 112);
            this.studentAddressTextBox.Name = "studentAddressTextBox";
            this.studentAddressTextBox.Size = new System.Drawing.Size(100, 23);
            this.studentAddressTextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(448, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "First Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(449, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Last Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date of birth";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(449, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Address";
            // 
            // updateStudentButton
            // 
            this.updateStudentButton.Location = new System.Drawing.Point(490, 170);
            this.updateStudentButton.Name = "updateStudentButton";
            this.updateStudentButton.Size = new System.Drawing.Size(138, 23);
            this.updateStudentButton.TabIndex = 11;
            this.updateStudentButton.Text = "Update Student";
            this.updateStudentButton.UseVisualStyleBackColor = true;
            this.updateStudentButton.Click += new System.EventHandler(this.updateStudentButton_Click);
            // 
            // deleteStudentButton
            // 
            this.deleteStudentButton.Location = new System.Drawing.Point(490, 199);
            this.deleteStudentButton.Name = "deleteStudentButton";
            this.deleteStudentButton.Size = new System.Drawing.Size(138, 23);
            this.deleteStudentButton.TabIndex = 12;
            this.deleteStudentButton.Text = "Delete Student";
            this.deleteStudentButton.UseVisualStyleBackColor = true;
            this.deleteStudentButton.Click += new System.EventHandler(this.deleteStudentButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1438, 672);
            this.Controls.Add(this.deleteStudentButton);
            this.Controls.Add(this.updateStudentButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.studentAddressTextBox);
            this.Controls.Add(this.studentDateOfBirthTextBox);
            this.Controls.Add(this.studentLastNameTextBox);
            this.Controls.Add(this.studentFirstNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.studentDbList);
            this.Controls.Add(this.createStudentButton);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button createStudentButton;
        private ListBox studentDbList;
        private Label label1;
        private TextBox studentFirstNameTextBox;
        private TextBox studentLastNameTextBox;
        private TextBox studentDateOfBirthTextBox;
        private TextBox studentAddressTextBox;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Button updateStudentButton;
        private Button deleteStudentButton;
    }
}