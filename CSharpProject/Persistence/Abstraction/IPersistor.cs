﻿namespace CSharpProject.Persistence.Abstraction
{
    public interface IPersistor<T>
    {
        IEnumerable<T> GetAll();
        T Create(T obj);
        T Update(string id, T obj);
        bool Delete(string id);
    }
}
