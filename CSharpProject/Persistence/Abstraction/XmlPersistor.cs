﻿using CSharpProject.Entities.Abstraction;
using CSharpProject.Helpers;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CSharpProject.Persistence.Abstraction
{
    public abstract class XmlPersistor<T> : IPersistor<T> where T : IdedEntity
    {
        private XmlSerializer _serializer = new XmlSerializer(typeof(T));
        public IEnumerable<T> GetAll()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(GetType().Name);
            var files = directoryInfo.GetFiles();
            return files.Select(x => Deserialize(File.ReadAllText(x.FullName)));
        }

        public T Create(T obj)
        {
            return CreateWithId(obj, IdGenerator.Generate());
        }

        private T CreateWithId(T obj, string id)
        {
            obj.Id = id;
            var xml = Serialize(obj);
            Directory.CreateDirectory($"{GetType().Name}");
            File.WriteAllText($"{GetType().Name}/{obj.Id}.xml", xml);
            return obj;
        }

        public bool Delete(string id)
        {
            File.Delete($"{GetType().Name}/{id}.xml");
            return true;
        }

        public T Update(string id, T obj)
        {
            Delete(id);
            CreateWithId(obj, id);
            return obj;
        }

        private string? Serialize(T obj)
        {
            using (var stringWriter = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(stringWriter))
                {
                    _serializer.Serialize(writer, obj);
                    return stringWriter.ToString();
                }
            }
        }

        private T Deserialize(string xml)
        {
            using (var stream = new MemoryStream(Encoding.Unicode.GetBytes(xml)))
            {
                return (T)_serializer.Deserialize(stream)!;
            }
        }
    }
}
